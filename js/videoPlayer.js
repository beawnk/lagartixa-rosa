//Api do youtube
var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player1;
var player2;
var player3;
var player4;
//Chamando vídeo
function  onYouTubeIframeAPIReady() {
  player1 = new YT.Player('ytplayer1', {
    height: '100%',
    width: '100%',
    videoId: 'mEt8_RT17mU',
    playerVars: {
      controls: 2,
      modestbranding: 0,
    },
    events: {
    //   'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
  player2 = new YT.Player('ytplayer2', {
    height: '100%',
    width: '100%',
    videoId: 'uT9qwZ_iNnM',
    events: {
    //   'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
  player3 = new YT.Player('ytplayer3', {
    height: '100%',
    width: '100%',
    videoId: 'yDt3OdJoVcI',
    events: {
    //   'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
  player4 = new YT.Player('ytplayer4', {
    height: '100%',
    width: '100%',
    videoId: 'prjKyBeKoD0',
    events: {
    //   'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
} 
function onPlayerReady(event) {
  event.target.playVideo();
}
function onPlayerStateChange(event) {
  for (let i = 0; i <= 4; i++) {
    playPause(event, '#ytplayer'+[i]+'');
    playPause(event, '#ytplayer'+[i]+'');
    playPause(event, '#ytplayer'+[i]+'');
    playPause(event, '#ytplayer'+[i]+'');
  }
}
function playPause(player, id) {
  if (player.data == YT.PlayerState.PLAYING) {
    $(id).next().fadeOut();
  }else {
    $(id).next().fadeIn();
  }
}
$('#ytplayer1').next().on('click', function(){
  clickPlay(player1);
})
$('#ytplayer2').next().on('click', function(){
  clickPlay(player2);
})
$('#ytplayer3').next().on('click', function(){
  clickPlay(player3);
})
$('#ytplayer4').next().on('click', function(){
  clickPlay(player4);
})
function clickPlay(player){
  if (!player.data == YT.PlayerState.PLAYING) {
    player.playVideo();
  }
}