let topButton = document.getElementById('button-top');

window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        topButton.style.display = "block";
    } else {
        topButton.style.display = "none";
    }
}
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}
const swiper1 = new Swiper('.video-carousel', {
    // Optional parameters
    loop: false,
    slidesPerView: 1.5,
    // slidesPerGroupAuto: true,
    spaceBetween: 30,
    centeredSlides: true,
    centeredSlidesBounds: false,
    // slidesPerGroup: 1,
    breakpoints: {
      200: {
        slidesPerView: 1,
        centeredSlides: false,
      },
      400: {
        slidesPerView: 1,
        spaceBetween: -20,
        centeredSlides: false,
      },
      1300: {
        slidesPerView: 1.2,
        spaceBetween: 30,
        centeredSlides: true,
      },
      1900: {
        slidesPerView: 1.5,
        spaceBetween: 30,
        centeredSlides: true,
      }
    },
    // Navigation arrows
    navigation: {
      nextEl: '.swiper-next-video',
      prevEl: '.swiper-prev-video',
    },
});
const swiper2 = new Swiper('.depo_carousel', {
  // Optional parameters
  loop: true,
  slidesPerView: 'auto',
  // slidesPerGroupAuto: true,
  spaceBetween: 50,
  centeredSlides: true,
  centeredSlidesBounds: true,
  // slidesPerGroup: 1,

  // Navigation arrows
  navigation: {
    nextEl: '.swiper-next2',
    prevEl: '.swiper-prev2',
  },
});
const swiper3 = new Swiper('.shop-swiper', {
  // Optional parameters
  loop: false,
  slidesPerView: 1.3,
  spaceBetween: 20,
  centeredSlides: false,
  slidesOffsetAfter: 40,
  breakpoints: {
    700: {
      slidesPerView: 1.3,
      centeredSlides: false,
      slidesOffsetAfter: 40,
    },
    800: {
      slidesPerView: 2.5,
      centeredSlides: false,
      spaceBetween: 40,
      slidesOffsetAfter: 40,
    },
    1000: {
      slidesPerView: 3,
      centeredSlides: false,
      spaceBetween: 40,
      slidesOffsetAfter: 0,
    },
    1300: {
      slidesPerView: 3,
      centeredSlides: false,
      spaceBetween: 40,
      slidesOffsetAfter: 0,
    }
  }
});
// $('.play').parent().click(function () {
//   if($(this).children(".play").get(0).paused){        
//     $(this).children(".play").get(0).play();   $(this).children(".playpause").fadeOut(); 
//     $(this).children(".play").attr('controls', 'checked');
//   }else{       
//     $(this).children(".play").get(0).pause();
//     $(this).children(".playpause").fadeIn();
//     $(this).children(".play").removeAttr('controls');
//     }
// });
